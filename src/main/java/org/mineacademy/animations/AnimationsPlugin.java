package org.mineacademy.animations;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;
import org.mineacademy.animations.command.AnimationsCommand;
import org.mineacademy.animations.listener.PlayerListener;
import org.mineacademy.animations.renderer.RendererTask;
import org.mineacademy.animations.sequence.Sequence;
import org.mineacademy.animations.tool.LaserPointerTool;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

/**
 * This plugin uses PluginTemplate, a simple template you can use every time you make
 * a new plugin. This will save you time because you no longer have to
 * recreate the same skeleton and features each time.
 * <p>
 * It uses Foundation for fast and efficient development process.
 */
public final class AnimationsPlugin extends SimplePlugin {

	/**
	 * Automatically perform login once when the plugin starts.
	 */
	@Override
	protected void onPluginStart() {
	}

	/**
	 * Automatically perform login when the plugin starts and each time it is reloaded.
	 */
	@Override
	protected void onReloadablesStart() {
		registerCommand(new AnimationsCommand());
		registerEvents(PlayerListener.getInstance());

		Common.runTimer(1, new AnimationTask());
		Common.runTimer(1, new RendererTask());
	}

	/**
	 * Automatically perform logic when the plugin is reloaded.
	 */
	@Override
	protected void onPluginReload() {
		Sequence.reload();
	}

	/**
	 * Automatically perform logic when the plugin is being disabled.
	 */
	@Override
	protected void onPluginStop() {
		Sequence.reload();
	}

	/* ------------------------------------------------------------------------------- */
	/* Events (your main class listens for events automatically) */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Demonstration of laser beam and kill aura
	 *
	 * @param event
	 */
	@EventHandler
	public void onClick(final PlayerInteractEvent event) {

		// Since MC 1.9, Bukkit fires two events, one for the main hand and
		// one for the off hand. We only want to process clicking from main hand.
		if (!Remain.isInteractEventPrimaryHand(event))
			return;

		if (event.getAction() != Action.RIGHT_CLICK_AIR)
			return;

		final Player player = event.getPlayer();

		// Uncomment to make any clicking target entities nearby and shoot at them
		/*final double radius = 20;
		final double arrowSpeed = 2;

		for (final Entity nearby : player.getNearbyEntities(radius, radius, radius)) {
			if (nearby.getType() != EntityType.PLAYER && nearby instanceof LivingEntity) {
				final Vector velocity = nearby.getLocation().toVector().subtract(player.getLocation().toVector()).normalize().multiply(arrowSpeed);
				player.launchProjectile(Arrow.class, velocity);

				// Uncomment to allow "kill aura" targeting all nearby entities.
				//break;
			}
		}*/

		// Make clicking with Laser Pointer tool create explosion at the end of laser beam
		if (LaserPointerTool.getInstance().isTool(player.getItemInHand())) {
			final Location loc = player.getLocation();
			final int length = 50; // show 50 blocks ahead

			for (double waypoint = 1; waypoint < length; waypoint += 0.5) {
				final Vector vector = loc.getDirection().multiply(waypoint);
				loc.add(vector);
				final Block block = loc.getBlock();

				if (!CompMaterial.isAir(block)) {
					player.getWorld().createExplosion(loc, 5F);

					// Clean up entities nearby to prevent lag
					Common.runLater(3, () -> {
						for (final Entity nearby : Remain.getNearbyEntities(loc, 5))
							if (nearby instanceof Creature)
								nearby.remove();
					});

					CompSound.ENTITY_GENERIC_EXPLODE.play(loc);
					CompParticle.EXPLOSION_HUGE.spawn(loc);

					break;
				}
			}
		}
	}

	/* ------------------------------------------------------------------------------- */
	/* Static */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Return the instance of this plugin, which simply refers to a static
	 * field already created for you in SimplePlugin but casts it to your
	 * specific plugin instance for your convenience.
	 *
	 * @return
	 */
	public static AnimationsPlugin getInstance() {
		return (AnimationsPlugin) SimplePlugin.getInstance();
	}
}
