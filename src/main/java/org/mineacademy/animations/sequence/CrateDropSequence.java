package org.mineacademy.animations.sequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Giant;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.animations.hologram.SimpleAnimatedHologram;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.SimpleHologram;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.CompProperty;
import org.mineacademy.fo.remain.Remain;

/**
 * Represents a sample crate drop sequence with many scenes.
 */
public final class CrateDropSequence extends Sequence {

	/**
	 * The giant entity we spawn throughout this sequence stored
	 * here for convenient use.
	 */
	private Giant giant;

	/**
	 * The items in the circle around the giant we spawn during this
	 * sequence stored here for convenient use.
	 */
	private List<SimpleAnimatedHologram> circleItems = new ArrayList<>();

	protected CrateDropSequence() {
		super("crate-drop");
	}

	/*
	 * Start this sequence
	 */
	@Override
	protected void onStart() {
		this.getLastLocation().add(0.5, 0, 0.5);

		// Step 1
		this.lightning();
		this.glowingStand(CompMaterial.CHEST);

		this.nextSequence(this::rotate);
	}

	/*
	 * Clean up the sequence when stopping prematurelly (such as when reloading).
	 */
	@Override
	public void disable() {
		if (this.giant != null)
			this.giant.remove();

		for (final SimpleHologram stand : this.circleItems)
			stand.remove();

		this.circleItems.clear();
		this.removeLast();
	}

	private void rotate() {

		// Step 2
		this.lightning();
		this.getLastLocation().add(0, 0.4, 0);
		this.getLastStand().setAnimated(true);

		this.nextSequence(this::giant);
	}

	private void giant() {

		// Step 3
		this.removeLast();

		this.giant = this.getLastLocation().getWorld().spawn(this.getLastLocation().add(2, -6.1, -4.5), Giant.class);

		CompProperty.AI.apply(this.giant, false);
		CompProperty.GRAVITY.apply(this.giant, false);
		CompProperty.COLLIDABLE.apply(this.giant, false);

		Remain.setInvisible(this.giant, true);

		this.giant.getEquipment().setItemInHand(new ItemStack(CompMaterial.CHEST.getMaterial()));
		this.giant.getWorld().playEffect(this.giant.getLocation().add(0, 1.7, 0), Effect.MOBSPAWNER_FLAMES, 0);

		this.getLastLocation().add(0, 1.5, 0);

		this.nextSequence(this::circleItems);
	}

	private void circleItems() {

		// Step 4
		final Location chestLocation = this.getLastStand().getLocation().clone().add(0, 2, 0);
		final double itemY = chestLocation.getY() + 2;

		final List<Integer> degrees = Arrays.asList(45, 90, 135, 180, 225, 270, 315, 360);
		this.circleItems = new ArrayList<>();

		for (int i = 0; i < degrees.size(); i++) {
			final int degree = degrees.get(i);
			final int circleRadius = 5;

			final double radians = Math.toRadians(degree);
			final double x = Math.cos(radians) * circleRadius;
			final double z = Math.sin(radians) * circleRadius;

			final boolean lastDegree = i + 1 == degrees.size();

			Common.runLater(20 + i * 20, () -> {

				final Location higherPosition = chestLocation.clone().add(x, 0, z);
				higherPosition.setY(itemY);

				this.setLastLocation(chestLocation);
				this.animatedStand(CompMaterial.CHEST);

				this.getLastStand().addParticleEffect(CompParticle.VILLAGER_HAPPY);

				this.getLastStand().teleport(higherPosition);

				final Entity entity = this.getLastStand().getEntity();

				Common.runTimer(2, new BukkitRunnable() {
					@Override
					public void run() {
						if (!entity.isValid()) {
							this.cancel();

							return;
						}

						if (MinecraftVersion.atLeast(MinecraftVersion.V.v1_16))
							entity.getWorld().playEffect(entity.getLocation().add(0, 1.7, 0), Effect.SMOKE, BlockFace.DOWN);
						else
							entity.getWorld().playEffect(entity.getLocation().add(0, 1.7, 0), Effect.SMOKE, 6);
					}
				});

				this.circleItems.add(this.getLastStand());

				if (lastDegree)
					this.nextSequence(this::dropItems);
			});
		}
	}

	private void dropItems() {

		// Step 5
		for (int i = 0; i < this.circleItems.size(); i++) {
			final SimpleHologram circleItem = this.circleItems.get(i);
			final boolean lastCircleItem = i + 1 == this.circleItems.size();

			Common.runLater(20 + i * 20, () -> {
				this.getLastLocation().getWorld().dropItem(circleItem.getLocation(), ItemCreator.of(CompMaterial.DIAMOND, "Special Diamond").make());

				if (lastCircleItem)
					this.nextSequence(this::cleanUp);
			});
		}
	}

	private void cleanUp() {

		// Step 6
		for (int i = 0; i < this.circleItems.size(); i++) {
			final SimpleHologram circleItem = this.circleItems.get(i);
			final boolean lastCircleItem = i + 1 == this.circleItems.size();

			Common.runLater(20 + i * 20, () -> {

				circleItem.getEntity().getWorld().playEffect(circleItem.getEntity().getLocation().add(0, 1.7, 0), Effect.MOBSPAWNER_FLAMES, 0);
				circleItem.remove();

				if (lastCircleItem) {
					this.giant.remove();
					this.giant.getWorld().playEffect(this.giant.getLocation().add(0, 1.7, 0), Effect.MOBSPAWNER_FLAMES, 0);

					Common.broadcast(Messenger.getSuccessPrefix() + "Sequence finished.");
				}
			});
		}

		this.circleItems.clear();
	}
}
