package org.mineacademy.animations.nofomenu;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import lombok.Data;

@Data
public abstract class Button {

	private final int slot;

	public abstract void onClick(Player player);

	public abstract ItemStack getItem();
}
