package org.mineacademy.animations.nofomenu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Menu {

	@Getter
	private final List<Button> buttons = new ArrayList<>();

	private final int size;

	public void addButton(Button button) {
		buttons.add(button);
	}

	public void onClick(int slot, ItemStack item) {

	}

	public final void displayTo(Player player) {
		final Inventory inventory = Bukkit.createInventory(player, this.size);

		for (final Button button : buttons)
			inventory.setItem(button.getSlot(), button.getItem());

		player.openInventory(inventory);
	}
}
