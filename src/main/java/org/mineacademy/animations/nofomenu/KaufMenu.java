package org.mineacademy.animations.nofomenu;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;

public class KaufMenu extends Menu {

	public KaufMenu() {
		super(9 * 3);

		this.addButton(new Button(16) {

			@Override
			public void onClick(Player player) {
				System.out.println("I clicked the damn thing");
			}

			@Override
			public ItemStack getItem() {
				return ItemCreator.of(CompMaterial.APPLE).make();
			}
		});
	}
}
