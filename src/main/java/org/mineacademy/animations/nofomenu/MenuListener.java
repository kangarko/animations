package org.mineacademy.animations.nofomenu;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class MenuListener implements Listener {

	@EventHandler
	public void onMenuClick(final InventoryClickEvent event) {

		final Player player = (Player) event.getWhoClicked();
		final int slot = event.getSlot();

		if (player.hasMetadata("Menu")) {
			final Menu menu = (Menu) player.getMetadata("Menu").get(0).value();

			for (final Button button : menu.getButtons())
				if (button.getSlot() == slot)
					button.onClick(player);

			menu.onClick(event.getSlot(), event.getCurrentItem());
		}
	}
}
