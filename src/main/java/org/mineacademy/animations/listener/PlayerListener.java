package org.mineacademy.animations.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * A sample listener for events.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PlayerListener implements Listener {

	/**
	 * The singleton instance
	 */
	@Getter
	private static final PlayerListener instance = new PlayerListener();

	/**
	 * Listen for armor stand being manipulated by players (typically right clicking)
	 * and prevent it if the stand is played by a scene.
	 *
	 * @param event
	 */
	@EventHandler
	public void onClick(final PlayerArmorStandManipulateEvent event) {
		if (event.getRightClicked().hasMetadata("AnimatedStand"))
			event.setCancelled(true);
	}

	/**
	 * Listen for armor stand being damaged by players (typically left clicking)
	 * and prevent it if the stand is played by a scene.
	 *
	 * @param event
	 */
	@EventHandler
	public void onDamage(final EntityDamageByEntityEvent event) {
		// ARMOR_STAND as string for compatiblity with older MC where it does not exist
		if (event.getEntityType().toString().equals("ARMOR_STAND") && event.getEntity().hasMetadata("AnimatedStand"))
			event.setCancelled(true);
	}
}
