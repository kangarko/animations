package org.mineacademy.animations.command;

import java.util.List;

import org.mineacademy.animations.PlayerCache;
import org.mineacademy.animations.menu.AnimatedButtonMenu;
import org.mineacademy.animations.menu.AnimatedMenu;
import org.mineacademy.animations.renderer.Renderer;
import org.mineacademy.animations.sequence.Sequence;
import org.mineacademy.animations.tool.ChestTool;
import org.mineacademy.animations.tool.LaserPointerTool;
import org.mineacademy.animations.tool.SceneTool;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;

/**
 * A sample command to display animations.
 */
public final class AnimationsCommand extends SimpleCommand {

	/**
	 * Create a new standalone command /sample
	 */
	public AnimationsCommand() {

		// The labels that trigger this command.
		super("animations/anim");

		// This is purely aesthetic and will be displayed when you type "/animations ?"
		setUsage("<play/toggle/tool/see/menu/menu2> [sequence/particle]");

		// This prevents running "/animations" with less than X arguments and shows help instead.
		setMinArguments(1);
	}

	/**
	 * Perform the main command logic.
	 */
	@Override
	protected void onCommand() {

		// Automatically prevent console from executing this command, so you can use "getPlayer()" below without checking "ifPlayer()"
		checkConsole();

		final String param = args[0].toLowerCase();
		final String name = args.length > 1 ? args[1] : null;
		final PlayerCache cache = PlayerCache.from(getPlayer());

		if ("play".equals(param)) {
			checkNotNull(name, "Specify which animation to play: " + Common.join(Sequence.getSequenceNames()) + ".");

			final Sequence sequence = Sequence.getByName(name);
			checkNotNull(sequence, "Invalid sequence '{1}', available: " + Common.join(Sequence.getSequenceNames()) + ".");

			sequence.start(getPlayer().getLocation());

		} else if ("toggle".equals(param)) {

			// Regardless of any argument typed, when there's already an animation, just cancel it
			if (cache.getRenderer() != null) {
				cache.setRenderer(null);

				// I changed returnTell to returnSuccess so that you won't see an error prefix in the message
				tellSuccess("No longer seeing rendered particle effects.");
				return;
			}

			checkNotNull(name, "Specify which render to toggle: " + Common.join(Renderer.getRendererNames()) + ".");

			final Renderer renderer = Renderer.getByName(name);
			checkNotNull(renderer, "Invalid sequence '{1}', available: " + Common.join(Renderer.getRendererNames()) + ".");

			cache.setRenderer(renderer);
			tellSuccess("Now seeing " + renderer.getName() + " particle effects.");

		} else if ("tool".equals(param)) {
			LaserPointerTool.getInstance().give(getPlayer());
			SceneTool.getInstance().give(getPlayer());
			ChestTool.getInstance().give(getPlayer());

		} else if ("see".equals(param)) {
			final boolean has = cache.isSeeingCircle();

			cache.setSeeingCircle(!has);
			tell((has ? "No longer" : "Now") + " seeing circle in front of you.");

		} else if ("menu".equals(param))
			new AnimatedMenu().displayTo(getPlayer());

		else if ("menu2".equals(param))
			new AnimatedButtonMenu().displayTo(getPlayer());
	}

	/**
	 * @see org.mineacademy.fo.command.SimpleCommand#tabComplete()
	 */
	@Override
	protected List<String> tabComplete() {

		switch (args.length) {
			case 1:
				return completeLastWord("play", "toggle", "tool", "see", "menu", "menu2");

			case 2:
				return completeLastWord("play".equals(args[0]) ? Sequence.getSequenceNames()
						: "toggle".equals(args[0]) ? Renderer.getRendererNames() : NO_COMPLETE);
		}

		return NO_COMPLETE;
	}
}
