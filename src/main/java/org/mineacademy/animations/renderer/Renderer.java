package org.mineacademy.animations.renderer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.util.Vector;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.remain.CompParticle;

import lombok.AccessLevel;
import lombok.Getter;

/**
 * Represents a series of particles being shown rapidly
 * in a skewed cirle creating an illusion of a persistent render.
 */
public abstract class Renderer {

	/**
	 * The default particle size used in {@link #spawnParticle(Location)},
	 * only applies on MC 1.13 or greater.
	 */
	private static final float DEFAULT_PARTICLE_SIZE = 0.6F;

	/**
	 * The registry of static fields initialized at first call of this class.
	 */
	private static final Map<String, Renderer> byName = new HashMap<>();

	/**
	 * The butterfly effect singleton instance
	 */
	public static final Renderer BUTTERFLY = new ButterflyRenderer();

	/**
	 * The wings effect singleton instance
	 */
	public static final Renderer WINGS = new WingsRenderer();

	/**
	 * The spiral effect singleton instance
	 */
	public static final Renderer SPIRAL = new SpiralRenderer();

	/**
	 * The name of this render
	 */
	@Getter
	private final String name;

	/**
	 * The particle used in this render
	 */
	private final CompParticle particle;

	/**
	 * The color used in this render
	 */
	private final Color color;

	/*
	 * Stores the location after render() call
	 */
	@Getter(value = AccessLevel.PROTECTED)
	private Location location;

	/*
	 * Creates a new instance of a rendered particle and registeres it.
	 */
	protected Renderer(final String name, final CompParticle particle, final Color color) {
		byName.put(name, this);

		this.name = name;
		this.particle = particle;
		this.color = color;
	}

	/**
	 * Spawns particles at the given location. This should be called
	 * repeatedly because the MC client removes the particles after a while.
	 *
	 * @param location
	 */
	public final void render(final Location location) {
		this.adjustLocation(location);
		this.location = location;

		int iterations = 0;

		// Draw a basic circle that is skewed by a custom calculation
		for (double degree = 0; degree < this.getMaxDegree(); degree += this.getParticleDensity()) {
			final double radians = Math.toRadians(degree);
			final Vector particleLocation = this.calculateParticleLocation(radians);

			final double delay = this.getDelay(iterations);
			final Location destination = this.location.clone().add(particleLocation);

			if (delay == -1)
				this.spawnParticle(destination);

				// Optional: Delay each particle to be shown after another
			else
				Common.runLaterAsync((int) Math.round(delay), () -> this.spawnParticle(destination));

			iterations++;
		}
	}

	/**
	 * Adjust the starting location from which we'll render this effect,
	 * by default we move the location from the middle head of the player
	 * down and behind him, and we stop animation from rotating vertically
	 * depending on where the player is looking up/down.
	 *
	 * @param location
	 */
	protected void adjustLocation(final Location location) {
		location.add(location.getDirection().normalize().multiply(-0.3)); // move behind the player
		location.subtract(0, 0.3, 0); // push down to chest
		location.setPitch(0F); // stop vertical rotation, only make particles rotate to sides, not up and down
	}

	/**
	 * Return the max degree of the circle we paint particles in
	 *
	 * @return
	 */
	protected abstract double getMaxDegree();

	/**
	 * Optional: Return a non -1 number that will make the particle at
	 * the given for-loop iteration be shown later.
	 *
	 * @param iteration
	 * @return
	 */
	protected double getDelay(final int iteration) {
		return -1;
	}

	/**
	 * Return how far each particle in the circle is from each other
	 *
	 * @return
	 */
	protected double getParticleDensity() {
		return 2;
	}

	/**
	 * Calculates the x and z location of a particle at the given angle in circle in radians
	 *
	 * @param radians
	 * @return
	 */
	protected abstract Vector calculateParticleLocation(double radians);

	/**
	 * A helper method to spawn a particle
	 *
	 * @param location
	 */
	protected final void spawnParticle(final Location location) {
		this.spawnParticle(location, DEFAULT_PARTICLE_SIZE);
	}

	/**
	 * A helper method to spawn a particle
	 *
	 * @param location
	 * @param size
	 */
	protected final void spawnParticle(final Location location, final float size /*MC 1.13+*/) {
		this.particle.spawn(location, this.color /* TODO I inserted the color here post-recording */, size);
	}

	/**
	 * Return a renderer by name, or null if it does not exist
	 *
	 * @param name
	 * @return
	 */
	public static Renderer getByName(final String name) {
		return byName.get(name);
	}

	/**
	 * Return all renderer names
	 *
	 * @return
	 */
	public static Set<String> getRendererNames() {
		return byName.keySet();
	}
}
