package org.mineacademy.animations.renderer;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.animations.PlayerCache;
import org.mineacademy.fo.remain.Remain;

/**
 * A repeating task that renders animations around players that have enabled one.
 */
public final class RendererTask extends BukkitRunnable {

	@Override
	public void run() {
		for (final Player player : Remain.getOnlinePlayers()) {
			final PlayerCache cache = PlayerCache.from(player);
			final Renderer renderer = cache.getRenderer();

			// Only draw the circle if the player has enabled this in his settings through /anim see command.
			if (renderer != null)
				renderer.render(player.getEyeLocation());
		}
	}
}
