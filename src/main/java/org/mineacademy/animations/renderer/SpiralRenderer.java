package org.mineacademy.animations.renderer;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.util.Vector;
import org.mineacademy.fo.remain.CompParticle;

/**
 * An example spiral renderer.
 */
public final class SpiralRenderer extends Renderer {

	protected SpiralRenderer() {
		super("spiral", CompParticle.REDSTONE, Color.BLUE);
	}

	@Override
	protected double getParticleDensity() {
		return 0.5;
	}

	@Override
	protected double getMaxDegree() {
		return 180 + 45;
	}

	@Override
	protected double getDelay(final int iterations) {
		return iterations / 2D;
	}

	@Override
	protected void adjustLocation(final Location location) {
		location.setPitch(0F);
		location.setYaw(0F);
	}

	@Override
	protected Vector calculateParticleLocation(final double radians) {

		final double spiralAmount = 6;
		final double radius = 0.5;

		final double x = radians * Math.cos(spiralAmount * radians) * radius;
		final double z = radians * Math.sin(spiralAmount * radians) * radius;

		return new Vector(x, -radians + 2, z);
	}

}
