package org.mineacademy.animations.renderer;

import org.bukkit.Color;
import org.bukkit.util.Vector;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.remain.CompParticle;

public final class ButterflyRenderer extends Renderer {

	protected ButterflyRenderer() {
		super("butterfly", CompParticle.REDSTONE, Color.ORANGE);
	}

	@Override
	protected double getMaxDegree() {
		return 360;
	}

	@Override
	protected Vector calculateParticleLocation(final double radians) {

		final double wingSize = 0.6;
		final double circlesAmount = 3;

		final double circle = wingSize * Math.pow(Math.E, Math.cos(radians));
		final double radius = circle - Math.cos(circlesAmount * radians);

		final double x = Math.sin(radians) * radius;
		final double z = Math.cos(radians) * radius;

		final Vector vector = new Vector(x, 0, z);

		MathUtil.rotateAroundAxisX(vector, -90);
		MathUtil.rotateAroundAxisY(vector, this.getLocation().getYaw());

		return vector;
	}

}
