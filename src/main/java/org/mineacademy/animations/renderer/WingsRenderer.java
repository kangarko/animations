package org.mineacademy.animations.renderer;

import org.bukkit.Color;
import org.bukkit.util.Vector;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.remain.CompParticle;

/**
 * An example wings renderer.
 */
public final class WingsRenderer extends Renderer {

	protected WingsRenderer() {
		super("wing", CompParticle.REDSTONE, Color.RED);
	}

	@Override
	protected double getParticleDensity() {
		return 6;
	}

	@Override
	protected double getMaxDegree() {
		return 360 * 4;
	}

	@Override
	protected Vector calculateParticleLocation(double radians) {

		final double width = 1;

		final double x = (width - Math.sin(radians)) * Math.cos(radians * 0.25);
		final double z = Math.sin(radians) - 1;

		final Vector vector = new Vector(x, 0, z);

		MathUtil.rotateAroundAxisX(vector, -60);
		MathUtil.rotateAroundAxisY(vector, this.getLocation().getYaw());

		return vector;
	}

}
