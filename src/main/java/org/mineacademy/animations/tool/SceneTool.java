package org.mineacademy.animations.tool;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.animations.sequence.Sequence;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.BlockTool;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * An automatically registered tool you can use in the game
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SceneTool extends BlockTool {

	/**
	 * The singular tool instance
	 */
	@Getter
	private static final Tool instance = new SceneTool();

	/**
	 * The actual item stored here for maximum performance
	 */
	private ItemStack item;

	/**
	 * @see Tool#getItem()
	 */
	@Override
	public ItemStack getItem() {

		if (item == null)
			item = ItemCreator.of(
					CompMaterial.CHEST,
					"Scene",
					"",
					"Click a block to",
					"launch a scene on it.")
					.make();

		return item;
	}

	@Override
	protected void onBlockClick(final Player player, final ClickType click, final Block block) {
		Sequence.CRATE_DROP.start(block.getLocation());

	}

	/**
	 * Cancel the event so that we don't destroy blocks when selecting them
	 *
	 * @see Tool#autoCancel()
	 */
	@Override
	protected boolean autoCancel() {
		return true;
	}
}
