package org.mineacademy.animations.tool;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.BlockTool;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * An automatically registered tool you can use in the game
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ChestTool extends BlockTool {

	/**
	 * The singular tool instance
	 */
	@Getter
	private static final Tool instance = new ChestTool();

	/**
	 * The actual item stored here for maximum performance
	 */
	private ItemStack item;

	/**
	 * @see Tool#getItem()
	 */
	@Override
	public ItemStack getItem() {

		if (item == null)
			item = ItemCreator.of(
					CompMaterial.DIAMOND_PICKAXE,
					"Chest tool",
					"",
					"Click a chest to",
					"open or close it.")
					.make();

		return item;
	}

	@Override
	protected void onBlockClick(final Player player, final ClickType click, final Block block) {
		if (block.getType() != CompMaterial.CHEST.getMaterial()) {
			Messenger.error(player, "You have to right/left click a chest.");

			return;
		}

		if (click == ClickType.LEFT)
			Remain.sendChestClose(block);
		else
			Remain.sendChestOpen(block);
	}

	/**
	 * Cancel the event so that we don't destroy blocks when selecting them
	 *
	 * @see Tool#autoCancel()
	 */
	@Override
	protected boolean autoCancel() {
		return true;
	}
}
