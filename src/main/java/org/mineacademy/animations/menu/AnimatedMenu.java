package org.mineacademy.animations.menu;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.slider.ColoredTextSlider;
import org.mineacademy.fo.slider.ItemSlider;
import org.mineacademy.fo.slider.Slider;

/**
 * An example menu showing different animation possibilities
 */
public final class AnimatedMenu extends Menu {

	public AnimatedMenu() {

		// Set the title and the menu size when a new instance is created
		this.setTitle("Bananas have no bones");
		this.setSize(9 * 3);
	}

	@Override
	protected void onPostDisplay(final Player player) {

		// Use an example colored text slider to move colors in menu title
		final Slider<String> textSlider = ColoredTextSlider
				.from(this.getTitle())
				.width(10)
				.primaryColor("&8&l")
				.secondaryColor("&2&l");

		this.animateAsync(2, () -> {
			setTitle(textSlider.next());
		});

		// Use an example items slider to move a nether star from left to right in
		// the middle of the menu
		final Slider<List<ItemStack>> itemsSlider = ItemSlider.from(
				ItemCreator.of(CompMaterial.GRAY_STAINED_GLASS_PANE, ""),
				ItemCreator.of(CompMaterial.NETHER_STAR, "Slided Item!"))
				.width(9);

		this.animate(10, new MenuRunnable() {
			@Override
			public void run() {
				final List<ItemStack> items = itemsSlider.next();

				for (int i = 0; i < items.size(); i++)
					setItem(9 + i, items.get(i));

				// If the slider has moved the start to the last slot, cancel the animation
				if (items.get(items.size() - 1).getType() == CompMaterial.NETHER_STAR.getMaterial())
					this.cancel();
			}
		});
	}

	@Override
	public ItemStack getItemAt(final int slot) {

		// An example of returning a custom item
		//
		// Check out Core Training > Upgrading to Foundation for tutorials on creating
		// buttons you can actually click on.
		if (slot == 1)
			return ItemCreator.of(CompMaterial.APPLE, "Demo item").make();

		return NO_ITEM;
	}
}
