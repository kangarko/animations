package org.mineacademy.animations.menu;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.button.Button;
import org.mineacademy.fo.menu.button.StartPosition;
import org.mineacademy.fo.menu.button.annotation.Position;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;

/**
 * An example menu showing different an animated button
 */
@SuppressWarnings("unused")
public final class AnimatedButtonMenu extends Menu {

	@Position(9 + 2)
	private final Button positionedButton;

	@Position(start = StartPosition.CENTER)
	private final Button positionedOffsetButton;

	private final Button getSlotButton;

	public AnimatedButtonMenu() {
		this.setTitle("Animated button");
		this.setSize(9 * 3);

		// You can also dynamically generate buttons by registering them using our method
		for (int i = 0; i < 9; i++) {
			final int finalI = i;

			this.registerButton(new Button(i) {

				@Override
				public void onClickedInMenu(Player player, Menu menu, ClickType click) {
					animateTitle("Clicked me!");
				}

				@Override
				public ItemStack getItem() {
					return ItemCreator.of(
							CompMaterial.BLACK_STAINED_GLASS_PANE,
							"&6Test Button " + finalI,
							"",
							"&7Click me!")
							.make();
				}
			});
		}

		this.positionedButton = new Button() {

			@Override
			public void onClickedInMenu(Player player, Menu menu, ClickType click) {
				new PositionedMenu().displayTo(player);
			}

			@Override
			public ItemStack getItem() {
				return ItemCreator.of(
						CompMaterial.CLOCK,
						"&6Current Time And Date",
						"",
						"&7Value: &f" + TimeUtil.getFormattedDate(),
						"",
						"Click to open menu.")
						.make();
			}
		};

		this.positionedOffsetButton = new Button() {

			@Override
			public void onClickedInMenu(Player player, Menu menu, ClickType click) {
				animateTitle("Clicked me!");
			}

			@Override
			public ItemStack getItem() {
				return ItemCreator.of(
						CompMaterial.COMPASS,
						"&6World Time",
						"",
						"&7Value: &f" + getViewer().getWorld().getTime())
						.make();
			}
		};

		this.getSlotButton = new Button() {

			@Override
			public void onClickedInMenu(Player player, Menu menu, ClickType click) {
				new GetSlotMenu().displayTo(player);
			}

			@Override
			public ItemStack getItem() {
				return ItemCreator.of(
						CompMaterial.ENCHANTED_BOOK,
						"&6Epoch Time",
						"",
						"&7Value: &f" + System.currentTimeMillis(),
						"",
						"Click to open menu.")
						.make();
			}

			@Override
			public int getSlot() {
				return 9 + 6;
			}
		};
	}

	@Override
	protected void onPostDisplay(final Player player) {
		this.animate(1, this::redrawButtons); // Will call Button#getItem for all buttons to refresh the items
	}

	private final class PositionedMenu extends Menu {

		public PositionedMenu() {
			super(AnimatedButtonMenu.this);

			this.setTitle("@Position menu");
			this.setSize(9 * 3);
		}

		@Override
		protected String[] getInfo() {
			return new String[] {
					"This menu was opened by",
					"a button having @Position",
					"over it."
			};
		}
	}

	private final class GetSlotMenu extends Menu {

		public GetSlotMenu() {
			super(AnimatedButtonMenu.this);

			this.setTitle("getSlot menu");
			this.setSize(9 * 3);
		}

		@Override
		protected String[] getInfo() {
			return new String[] {
					"This menu was opened by a",
					"button returning getSlot()."
			};
		}
	}
}
