package org.mineacademy.animations;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import org.mineacademy.animations.tool.LaserPointerTool;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.Remain;

/**
 * A simple animation task to show circles or spheres around player
 */
@SuppressWarnings("unused")
public class AnimationTask extends BukkitRunnable {

	// Used to limit displaying animations only once per a time period.
	//
	// You are better of running the task slower than using this method for the entire task,
	// but you can use it if you need to split parts of your code for different intervals.
	private long elapsedTicks = 0;

	@Override
	public void run() {

		// Drawing a circle

		/*final int degreeStep = 2;
		final double circleRadius = 3;
		
		for (final Player player : Remain.getOnlinePlayers()) {
			final Location location = player.getLocation();
		
			for (int degree = 0; degree < 360; degree += degreeStep) {
				final double radians = Math.toRadians(degree);
		
				final double x = Math.cos(radians) * circleRadius;
				final double z = Math.sin(radians) * circleRadius;
		
				// Half circle
				//if (location.getY() + z < location.getY())
				//	continue;
		
				CompParticle.VILLAGER_HAPPY.spawn(location.clone().add(x, z, 0));
			}
		}*/

		// Drawing a sphere

		//for (final Player dude : Remain.getOnlinePlayers())
		//	this.drawSphere(dude);

		// Drawing a vector

		if (elapsedTicks++ % 2 != 0) // X.00
			return;

		for (final Player online : Remain.getOnlinePlayers()) {
			if (LaserPointerTool.getInstance().isTool(online.getItemInHand())) {
				final Location loc = online.getLocation().add(0, 1, 0);

				// show twenty blocks ahead
				// AVOID HIGH NUMBERS since they trigger chunk loading which leads to lag
				final int length = 20;

				for (double waypoint = 1; waypoint < length; waypoint += 0.5) {
					final Vector vector = loc.getDirection().multiply(waypoint);

					loc.add(vector);
					CompParticle.REDSTONE.spawn(loc);

					final Block block = loc.getBlock();

					if (!CompMaterial.isAir(block)) {
						loc.subtract(vector.normalize().multiply(2));

						CompParticle.REDSTONE.spawn(loc);
						this.drawCircle(loc);
					}
				}
			}
		}

		// Drawing a circle with rotation around player

		/*final int degreeStep = 1;
		final double circleRadius = 3;

		for (final Player player : Remain.getOnlinePlayers())

			// Only draw the circle if the player has enabled this in his settings through /anim see command.
			if (PlayerCache.from(player).isSeeingCircle()) {
				final Location location = player.getLocation().add(player.getLocation().getDirection().normalize());

				for (int degree = 0; degree < 360; degree += degreeStep) {
					final double radians = Math.toRadians(degree);

					final double x = Math.cos(radians) * circleRadius;
					final double z = Math.sin(radians) * circleRadius;

					final Vector vector = new Vector(x, 0, z);

					MathUtil.rotateAroundAxisX(vector, location.getPitch() + 90);
					MathUtil.rotateAroundAxisY(vector, location.getYaw());

					CompParticle.VILLAGER_HAPPY.spawn(location.clone().add(vector));
				}
			}*/
	}

	/*
	 * Draws a sphere around the player.
	 */
	private void drawSphere(final Player player) {
		final Location center = player.getEyeLocation();
		final double radius = 2.5;
		final double step = 10;

		for (double degree = 0; degree <= 180; degree += step) {
			final double radians = Math.toRadians(degree);

			final double x = Math.cos(radians) * radius;
			final double z = Math.sin(radians) * radius;

			this.drawCircle(center, step, x, z);
		}
	}

	/*
	 * Draws a circle around the player.
	 */
	private void drawCircle(final Location center) {
		final int radius = 3;

		for (double degree = 0; degree <= 360; degree += 10) {
			final double radians = Math.toRadians(degree);

			final double x = Math.cos(radians) * radius;
			final double z = Math.sin(radians) * radius;

			CompParticle.REDSTONE.spawn(center.clone().add(x, 0, z), Color.BLUE, 0.25F);
		}
	}

	/*
	 * Draws a circle around the player. Used when drawing a sphere.
	 */
	private void drawCircle(final Location center, final double step, final double height, final double radius) {
		for (double degree = 0; degree <= 360; degree += step) {
			final double radians = Math.toRadians(degree);

			final double x = Math.cos(radians) * radius;
			final double z = Math.sin(radians) * radius;

			CompParticle.REDSTONE.spawn(center.clone().add(x, height, z), RandomUtil.nextBoolean() ? Color.BLUE : Color.BLACK, 0.35F);
		}
	}
}
